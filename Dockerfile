
FROM python:alpine

#shit will fail if you don't set a user as a build-arg, using a uid
ARG UID=""

RUN mkdir -p /usr/weeb/anime

RUN adduser -Du $UID user

RUN chown -R $UID /usr/weeb/anime  

RUN apk add libxml2-dev libc-dev g++ libxslt-dev npm git vim

RUN pip install requests beautifulsoup4

RUN npm install webtorrent-cli -g

USER user

WORKDIR /usr/weeb/anime

COPY . /usr/weeb/anime
