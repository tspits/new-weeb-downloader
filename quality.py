import requests
from bs4 import BeautifulSoup as BS

from shows import SHOW_LIST_URL

import sys

if __name__=="__main__":

    print(SHOW_LIST_URL + sys.argv[1])
    soup = BS(requests.get(SHOW_LIST_URL + sys.argv[1]).text, features="html.parser")
    print(type(soup))
    load_more_links = soup.findAll("a", {"class":"load_more_links"})
    for load_more in load_more_links:
        try:
            load_more['href']
            continue
        except:
            print(f"Available : {load_more.text}")

    
