import requests
from bs4 import BeautifulSoup as BS

from shows import SHOW_LIST_URL
import re
from subprocess import call
import sys

# args : <show href> <quality>

if __name__=="__main__":

    soup = BS(requests.get(SHOW_LIST_URL + sys.argv[1]).text, features="html.parser")
    load_more_links = soup.findAll("a", {"class":"load_more_links"})
    p = re.compile(".*" + sys.argv[2] + ".*")
    for load_more in load_more_links:
        if load_more.text == "Magnet" and p.match(load_more["href"]):
            print("MATCHED")
            call(f"webtorrent \"{load_more['href']}\" -o \"anime\"",
                    shell=True)
        else:
            print("SAD")
            print(load_more.text)
