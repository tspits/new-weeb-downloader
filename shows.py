import requests
from bs4 import BeautifulSoup as BS

SHOW_LIST_URL="https://erai-raws.info/anime-list/"


if __name__=="__main__":

    r = requests.get(SHOW_LIST_URL)
    showsSoup = BS(requests.get(SHOW_LIST_URL).text, features="html.parser")
    show_wrapper = showsSoup.find('div', {"class":"shows-wrapper"})
    all_shows = show_wrapper.findAll("div", {"class":"ind-show button button5"})
    for show in all_shows:
        a = show.find('a')
        print(f"URL:{a['href']} title:{a['title']}")
